/*
    OpenDDA library API

    Copyright (C) 2020  Benjamin "Mogli" Mann

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OPEN_DDA
#define OPEN_DDA
#include <mpi.h>

#ifdef __cplusplus
extern "C" {
#endif

#include "dcomplex_basetype.h"

// target shape "ellipsoid"
#define DDA_ELLIPSOID "ellipsoid"
// target shape "cuboid"
#define DDA_CUBOID "cuboid"

/* notes:
   * scattering-angles:
   *  The azimuthal angle phi is the clockwise angle [0,360], in the [x_{L}y_{L}]-plane, between the scattering plane and the +x_{L}-axis looking in the +z_{L}-axis direction i.e. the angle between the scattering plane and the xz-plane. The scattering plane is the plane containing the incident direction and the scattering direction [+z_{L}-axis & n_{sca}]), phi=0 => scattering plane=xz-plane, phi=90 => scattering plane=yz-plane
   * The polar/zenith angle theta is the angle [0,180], in the scattering plane, between the incident direction +z_{L}-axis and the scattering direction n_{sca} theta=0 => forward scattering, scattering direction=+z_{L} axis theta=180 => backward scattering, scattering direction=-z_{L} axis
*/

/* required before first dda run;
   handles domain setup
   and iterative solver setup
   @param   comm           MPI communicator
   @param   iterative_scheme  name of scheme to be used. Choose from the above definitions!
   @param   preconditioning   0: no preconditioning, 1: point-Jacobi-preconditioning
   @param   starting_vectors  number of starting vectors; only used for DDA_ML_BICG_STAB_ORG, DDA_ML_BICG_STAB, DDA_ML_BICG_STAB_SS and DDA_R_BICG_STAB.
   @param   scatter        scattering directions in spherical cooridinates [phi, theta]
   @param   wavelength     incident wavelenght
   @param   e0             x and y component of incident polarization [E_{x},E_{y},0] (normalization handled internally)
   @param   domain_shape   either the name of a file containing the shape configuration or one of the predefined shapes (see above)
   @param   domain_size    number of dipoles in [X,Y,Z] direction
   @param   d              mesh width = space between dipoles
   @param   enable_timing  enable time measuring and corresponding output
*/
void dda_init(const MPI_Comm comm, const char* iterative_scheme, int preconditioning, int starting_vectors,
            const double scatter[2], double wavelength, const dcomplex e0[2],
            const char* domain_shape, const int domain_size[3], double d, int enable_timing);

// free all memory
void dda_free();

/* rotate target frame -- requires reassembling interaction matrix
   @param   phi   Azimuthal angle [0,360]. The clockwise angle in the [x_{L}y_{L}]-plane about the z_{L}-axis looking in the +z_{L} direction
   @param   theta Polar/zenith angle [1,-1] i.e. [0,180]. The clockwise angle in the [x^{phi}z^{phi}]-plane about the y^{phi}-axis looking in the +y^{phi}-axis direction
   @param   psi   Azimuthal angle [0,360]. The clockwise angle in the [x^{theta}y^{theta}]-plane about the z^{theta}-axis looking in the +z^{theta}-axis direction
*/
void dda_rotate_target(double phi, double theta, double psi);

/* specify incident field by means of normalized Jones vector and euler rotations of target frame -- requires reassembling interaction matrix
   @param   phi   Azimuthal angle [0,360]. The clockwise angle in the [x_{L}y_{L}]-plane about the z_{L}-axis looking in the +z_{L} direction
   @param   theta Polar/zenith angle [1,-1] i.e. [0,180]. The clockwise angle in the [x^{phi}z^{phi}]-plane about the y^{phi}-axis looking in the +y^{phi}-axis direction
   @param   psi   Azimuthal angle [0,360]. The clockwise angle in the [x^{theta}y^{theta}]-plane about the z^{theta}-axis looking in the +z^{theta}-axis direction
   @param   e0_x   x component of "normalized" Jones vector [E_{x},E_{y},0] (normalization handled here)
   @param   e0_y   y component of "normalized" Jones vector [E_{x},E_{y},0] (normalization handled here)
*/
void dda_set_incident_field(double phi, double theta, double psi, const dcomplex e0_x, const dcomplex e0_y);

/* set new parameters -- requires reassembling interaction matrix
   @param   wavelength  incident wavelenght
*/
void dda_set_wavelength(double wavelength);

// iterative scheme: Conjugate-Gradients
#define DDA_CG "cg"
// iterative scheme: Conjugate-Gradients Squared
#define DDA_CG_S "cgs"
// iterative scheme: BiConjugate-Gradients
#define DDA_BICG "bicg"
// iterative scheme: BiConjugate-Gradients for symmetric systems
#define DDA_BICG_SYM "bicg_sym"
// iterative scheme: BiConjugate-Gradients (stabilized version)
#define DDA_BICG_STAB "bicgstab"
// iterative scheme: BiCGSTAB variant based on multiple Lanczos starting vectors (Author's original algorithm)
#define DDA_ML_BICG_STAB_ORG "mlbicgstab_orig"
// iterative scheme: BiCGSTAB variant based on multiple Lanczos starting vectors (Author's reformulation)
#define DDA_ML_BICG_STAB "mlbicgstab"
// iterative scheme: BiCGSTAB variant based on multiple Lanczos starting vectors (Author's space saving algorithm)
#define DDA_ML_BICG_STAB_SS "mlbicgstab_ss"
// iterative scheme: Restarted, stabilised version of the BiConjugate-Gradients
#define DDA_R_BICG_STAB "rbicgstab"
// iterative scheme: Quasi-minimal residual with coupled two-term recurrences
#define DDA_QMR "qmr"
// iterative scheme: Quasi-minimal residual for symmetric systems
#define DDA_QMR_SYM "qmr_sym"
// iterative scheme: Transpose-free quasi-minimal residual
#define DDA_TF_QMR "tfqmr"

// initial guess: x=0
#define DDA_X0 0
// initial guess: x=1
#define DDA_X1 1
// initial guess: x=b, i.e., incident electric-field
#define DDA_XB 2
// initial guess: x=(1/polarisability)
#define DDA_XALPHA 3

/* Configure iterative solver.
   @param   convergence_tol   convergence tolerance
   @param   breakdown_tol     breakdown tolerance
   @param   max_iter          maximum number of iterations
   @param   initial_guess     type of initial guess. Choose from above definitions!
*/
void dda_configure_iterative_solver(double convergence_tol, double breakdown_tol, int max_iter, int initial_guess);

/* setup interaction matrix with with given material configuration
   @param   epsilon                    permittivity of the dipoles
   @param   orthogonal_polarization    if != 0, e1 = [0,0,1] ⨯ conj(e0) will be used instead of e0
*/
void dda_setup_interaction_matrix(dcomplex* epsilon, int orthogonal_polarization);

/* update interaction matrix with with given material configuration.
   @param   epsilon                    permittivity of the dipoles
*/
void dda_set_material_configuration(dcomplex* epsilon);

/* get integer coordinates x,y,z of the populated lattice sites
   @returns vector coord s.th. coord[3*n + i] corresponds to the x_i
         coordinate of the n-th dipole
*/
const int* dda_get_coordinates();

/* Compute polarization, i.e., solve AP=E.
   Requires matrix setup!
   @param   adjoint  if != 0: compute adjoint problem AP=conj(E)
*/
const dcomplex* dda_compute_P(int adjoint);

/* Compute cross-sections and print efficiencies to stdout -- requires that the system is solved.
*/
void dda_compute_cross_section_efficiencies();

/* get extinction cross section  -- requires that the system is solved.
*/
double dda_get_c_ext();

/* get absorbtion cross section  -- requires that the system is solved.
*/
double dda_get_c_abs();

/* get scattering cross section  -- requires that the system is solved.
*/
double dda_get_c_sca();

/* return pointer to dipole polarization  -- requires that the system is solved.
*/
const dcomplex* dda_getP();

/* return pointer to initial guess data for iterative solver, s.th. custom
   initial guess can be used.
   Calling this function overrides the initial_guess setting made in
   dda_configure_iterative_solver() and vice versa.
*/
dcomplex* dda_get_initialGuessPtr();

/*
   @param   conjugate if true: return conj(E)
   @returns pointer to electrical field.
*/
const dcomplex* dda_getE(int conjugate);

/* get dipole density
   @returns (3 d^3)/(4 pi) where d = distance between dipoles
*/
double dda_get_density();

// return number of dipoles on this MPI-rank
int dda_N_local();

// return total number of dipoles
int dda_N_global();

// return MPI rank
int MPI_rank();

// return number of MPI processes
int MPI_np();

// return local memory usage in bytes
size_t dda_local_memory_usage();

#ifdef __cplusplus
} // !extern "C"
#endif

#endif // !OPEN_DDA