/* Written by James Mc Donald 2006
   [Antispam: email in reverse] ei.yawlagiun.ti@semaj

   Reads in the user defined parameters using control.input

   Copyright (C) 2006 James Mc Donald
   Computational Astrophysics Laboratory
   National University of Ireland, Galway
   This code is covered by the GNU General Public License */
#include "read_parameters.h"

void read_parameters(void){
   int i,j,k,m,a,b;
   char string[200]={0},compare[200]={0},temp[60]={0};
   FILE *input,*data;

   if((input=fopen("control.input","r"))==NULL){
      print_error("File IO error","Could not open the file \"control.input\"",NULL);
   }

   while((fgets(string,sizeof(string),input))!=NULL){
      if(string[0]=='#'){ /* Ignore comments */
         continue;
      }
      else{
         i=-1;reset_string(compare);
         do{ /* Copy everything up to and including the '=' to compare string */
            i++;compare[i]=string[i];
         } while((compare[i]!='=')&&(string[i+1]!='\0')&&(string[i+1]!='\n'));
/* *********************************************************************************************************************************************
Target shape and target construction parameters */
         if(strcmp(compare,"Shape=")==0){
            /* Extract the shape information */
            while(((ispunct(string[i+1]))!=0)&&(string[i+1]!='-')){i++;} /* Skip all punctuation (except '-') */
            k=0;reset_string(compare);
            do{ /* Copy everything after '=' to '\n' to compare string,
               ignoring punctuation (except fullstop and minus) and spaces, tabs, newlines etc.. */
               if(((isspace(string[++i]))!=0)||(((ispunct(string[i]))!=0)&&(string[i]!='.')&&(string[i]!='-')&&(string[i]!='_'))){continue;}
               else{compare[k++]=(char)tolower(string[i]);}
            } while((string[i+1]!=',')&&(string[i+1]!='\0')&&(string[i+1]!='\n'));
            reset_string(target.shape);
            strncpy(target.shape,compare,strlen(compare));
            /* Check to see if the shape identifier is recognised OR is a valid file name */
            if((strcmp(target.shape,"ellipsoid")!=0)&&
               (strcmp(target.shape,"cuboid")!=0)){ /* If not a recognised shape identifier */
               if((data=fopen(target.shape,"r"))!=NULL){ /* Target data file exists */
                  target.from_file=1; /* Set flag to indicate that the target data is to be read from a file */
                  fclose(data);
               }
               else{ /* The specified identifier is NOT recognised AND is NOT a valid file name */
                  reset_string(string);
                  sprintf(string,"\'%s\' is not a recognised shape and is not the name of an accessible data file",target.shape);
                  print_error("Target shape description error",string,"Please check the \'control.input\' file");
               }
            }
            /* Read in the 6 target construction parameters from the control.input file */
            j=0;
            while(((ispunct(string[i+1]))!=0)&&(string[i+1]!='-')){i++;} /* Skip all punctuation (except '-') */
            for(m=0;m<6;m++){ /* Read in the 6 target construction parameters */
               k=0;reset_string(compare);
               do{ /* Copy everything after '=' OR ',' to ',' OR '\n' to compare string,
                  ignoring punctuation (except fullstop and minus) and spaces, tabs, newlines etc.. */
                  if(((isspace(string[++i]))!=0)||(((ispunct(string[i]))!=0)&&(string[i]!='.')&&(string[i]!='-'))){continue;}
                  else{compare[k++]=(char)toupper(string[i]);}
               } while((string[i+1]!=',')&&(string[i+1]!='\0')&&(string[i+1]!='\n'));
               if(strcmp(compare,"NULL")!=0){
                  j++;
                  target.construction_parameters[m]=atoi(compare); /* string to int conversion */
                  if(target.construction_parameters[m]<=0){
                     reset_string(string);
                     sprintf(string,"Target construction parameter %d MUST be > 0",m);
                     print_error("Target shape description error",string,"Please check the \'control.input\' file");
                  }
               }
               while(((ispunct(string[i+1]))!=0)&&(string[i+1]!='-')){i++;} /* Skip all punctuation (except '-') */
            }
            /* Check that the shape and construction parameters are consistent */
            if((strcmp(target.shape,"ellipsoid")==0)||(strcmp(target.shape,"cuboid")==0)){
               if(j<3){ /* The shape must be accompanied by the first 3 target construction parameters */
                  reset_string(string);
                  sprintf(string,"The \"%s\" shape MUST be accompanied by the first 3 target construction parameters",target.shape);
                  print_error("Target construction parameter error",string,"Please check the \'control.input\' file");
               }
            }
         }
/* *********************************************************************************************************************************************
Degree of the interpolating polynomial for the improved Akima method */
         else if(strcmp(compare,"Polynomial degree=")==0){
            while(((ispunct(string[i+1]))!=0)&&(string[i+1]!='-')){i++;} /* Skip all punctuation (except '-') */
            k=0;reset_string(compare);
            do{ /* Copy everything after '=' to '\n' to compare string,
               ignoring punctuation (except fullstop and minus) and spaces, tabs, newlines etc.. */
               if(((isspace(string[++i]))!=0)||(((ispunct(string[i]))!=0)&&(string[i]!='.')&&(string[i]!='-'))){continue;}
               else{compare[k++]=string[i];}
            } while(string[i+1]!='\n');
            degree=atoi(compare); /* string to int conversion */
            /* Check degree value */
            if(degree<3){
               print_error("Interpolation description error","The degree of the interpolating polynomial MUST be >= 3 [Default=3]","Please check the \'control.input\' file");
            }
         }
/* *********************************************************************************************************************************************
Timing */
         else if(strcmp(compare,"Timing=")==0){
            while(((ispunct(string[i+1]))!=0)&&(string[i+1]!='-')){i++;} /* Skip all punctuation (except '-') */
            k=0;reset_string(compare);
            do{ /* Copy everything after '=' to '\n' to compare string,
               ignoring punctuation (except fullstop and minus) and spaces, tabs, newlines etc.. */
               if(((isspace(string[++i]))!=0)||(((ispunct(string[i]))!=0)&&(string[i]!='.')&&(string[i]!='-'))){continue;}
               else{compare[k++]=string[i];}
            } while(string[i+1]!='\n');
            timing.enabled=atoi(compare); /* string to int conversion */
            /* Check value */
            if((timing.enabled!=0)&&(timing.enabled!=1)){
               print_error("Timing description error","The timing flag MUST be set to either 0 or 1","Please check the \'control.input\' file");
            }
         }
/* *********************************************************************************************************************************************
Output precision */
         else if(strcmp(compare,"Output precision [DOUBLE]=")==0){
            while(((ispunct(string[i+1]))!=0)&&(string[i+1]!='-')){i++;} /* Skip all punctuation (except '-') */
            k=0;reset_string(compare);
            do{ /* Copy everything after '=' to '\n' to compare string,
               ignoring punctuation (except fullstop and minus) and spaces, tabs, newlines etc.. */
               if(((isspace(string[++i]))!=0)||(((ispunct(string[i]))!=0)&&(string[i]!='.')&&(string[i]!='-'))){continue;}
               else{compare[k++]=string[i];}
            } while(string[i+1]!='\n');
            DBLP=atoi(compare); /* string to int conversion */
            /* Check DBLP value */
            if((DBLP<0)||(DBLP>DBL_DIG)){
               reset_string(string);
               sprintf(string,"The output precision MUST be in the range 0 to %d",DBL_DIG);
               print_error("Output precision description error",string,"Please check the \'control.input\' file");
            }
            if(DBLP==0){ /* if set to ZERO then set equal to the maximum */
               DBLP=DBL_DIG;
            }
         }
/* *********************************************************************************************************************************************
Distributed transpose algorithm */
         else if(strcmp(compare,"Distributed transpose algorithm=")==0){
            /* Extract the distributed transpose algorithm for the 6 independent tensor components of the interaction matrix */
            while(((ispunct(string[i+1]))!=0)&&(string[i+1]!='-')){i++;} /* Skip all punctuation (except '-') */
            k=0;reset_string(compare);
            do{ /* Copy everything after '=' to '\n' to compare string,
               ignoring punctuation (except fullstop and underscore) and spaces, tabs, newlines etc.. */
               if(((isspace(string[++i]))!=0)||(((ispunct(string[i]))!=0)&&(string[i]!='.')&&(string[i]!='_'))){continue;}
               else{compare[k++]=(char)tolower(string[i]);}
            } while((string[i+1]!=',')&&(string[i+1]!='\0')&&(string[i+1]!='\n'));
            parallel.tensor_transpose=atoi(compare); /* string to int conversion */
            /* Check tensor_transpose value */
            if((parallel.tensor_transpose<0)||(parallel.tensor_transpose>5)){
               print_error("Distributed tarnspose description error","The distributed tensor transpose algorithm MUST be in the range 0..5","Please check the \'control.input\' file");
            }
            /* Extract the distributed transpose algorithm for the 3 zero-padded vector components */
            while(((ispunct(string[i+1]))!=0)&&(string[i+1]!='-')){i++;} /* Skip all punctuation (except '-') */
            k=0;reset_string(compare);
            do{ /* Copy everything after '=' to '\n' to compare string,
               ignoring punctuation (except fullstop and underscore) and spaces, tabs, newlines etc.. */
               if(((isspace(string[++i]))!=0)||(((ispunct(string[i]))!=0)&&(string[i]!='.')&&(string[i]!='_'))){continue;}
               else{compare[k++]=string[i];}
            } while(string[i+1]!='\n');
            parallel.padded_transpose=atoi(compare); /* string to int conversion */
            /* Check padded_transpose value */
            if((parallel.padded_transpose<0)||(parallel.padded_transpose>5)){
               print_error("Distributed tarnspose description error","The distributed vector transpose algorithm MUST be in the range 0..5","Please check the \'control.input\' file");
            }
         }
/* *********************************************************************************************************************************************
ERROR */
         else{ /* Print error */
            j=0;
            k=(int)strlen(compare);
            for(i=0;i<k;i++){
               if((isspace(compare[i]))==0){j=1;} /* Character is not a space, tab, newline etc.. */
            }
            if(j==0){ /* Input line is blank */
               print_error("Parameter file input error","A forbidden blank line was encountered","Please check the \'control.input\' file");
            }
            else{
               reset_string(string);
               for(i=0;i<k;i++){
                  if(compare[i]=='\t'){ /* Replace tabs with spaces for correct error printing */
                     compare[i]=' ';
                  }
               }
               sprintf(string,"The input description string \"%s\" is invalid",compare);
               print_error("Parameter file input error",string,"Please check the \'control.input\' file");
            }
         }
      }
      reset_string(string);
   }
   fclose(input);
}