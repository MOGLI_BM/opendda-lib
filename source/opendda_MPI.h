/* Written by James Mc Donald 2006
   [Antispam: email in reverse] ei.yawlagiun.ti@semaj

   Function prototypes for opendda and global variables
   Note: see definitions.h for an explanation of the variables

   Copyright (C) 2006 James Mc Donald,
   Computational Astrophysics Laboratory,
   National University of Ireland, Galway
   This code is covered by the GNU General Public License */

#include <fftw3.h>
#include <mpi.h>

#include "attributes_type.h"
#include "bitfielding.h"
#include "broadcast_parameters.h"
#include "build_target.h"
#include "cross_sections_MPI.h"
#include "dcomplex_type.h"
#include "dda_interaction_matrix_MPI.h"
#include "definitions.h"
#include "dft_plan.h"
#include "dipole_polarisabilities_MPI.h"
#include "efficiencies.h"
#include "find_dft_size.h"
#include "incident_electric_field_MPI.h"
#include "interpolation.h"
#include "iterative_solution.h"
#include "memory_allocate_MPI.h"
#include "memory_allocation_sizes.h"
#include "memory_free_MPI.h"
#include "mpi_environment.h"
#include "reset_string.h"
#include "rotation.h"
#include "set_domain.h"
#include "set_initial_guess.h"
#include "timing_info.h"

/* Global variables */
MPI_Comm dda_mpi_comm;
int wl,er,f0,t0,p0,degree,dft_interaction_matrix_flag=0;
int polarisation_state,polarisations,DBLP, matrix_setup_complete, computation_complete;
double size_parameter,wavenumber,wavenumber_previous=-1.0, effective_radius, dda_density;
dcomplex *dipole_polarisation,*incident_E_field,*zero_padded_vector;
dcomplex *interaction_matrix,*interaction_matrix_diagonal,*point_jacobi,*xdft,*zdft;
dcomplex *xzscratch,two={{2.0,0.0}},one={{1.0,0.0}},onei={{0.0,1.0}},zero={{0.0,0.0}},minusone={{-1.0,0.0}};
fftw_plan plan_vcxdft,plan_vcydft,plan_vczdft,plan_vcxdfti,plan_vcydfti,plan_vczdfti;
fftw_plan plan_tcxdft,plan_tcydft,plan_tczdft;
attributes_iterative iterative={0};
attributes_target target={0};
attributes_incident_polarisation incident_LF={0},incident_TF={0};
attributes_scattering_vectors scattering_LF={0},scattering_TF={0};
attributes_cross_sections cross_section={0};
attributes_efficiency efficiency={0};
attributes_timing timing={0};
attributes_parallel parallel={0};
MPI_Op add_dcomplex; /* User-defined MPI function to add complex_numbers */
/* Iterative vectors */
dcomplex *c,*d,*g,*gam,*gamp,*gampp,*gtilde,*omega,*omegatilde,*p,*pold,*ptilde,*q;
dcomplex *r,*rtilde,*s,*sigma,**tau,*u,*utilde,*v,*vtilde,*ya,*yb,*zd,*zg,*zomega;
/* For SIMD oriented Fast Mersenne Twister RNG initialisation */
unsigned int *init;
int* coordinates;
size_t dda_total_memory_usage;