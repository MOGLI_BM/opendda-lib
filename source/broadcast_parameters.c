/* Written by James Mc Donald 2006
   [Antispam: email in reverse] ei.yawlagiun.ti@semaj

   Broadcasts parameters to the slave nodes

   Copyright (C) 2006 James Mc Donald,
   Computational Astrophysics Laboratory,
   National University of Ireland, Galway
   This code is covered by the GNU General Public License */
#include "broadcast_parameters.h"

void broadcast_control_parameters(void){

   long begin;
   int i,count=17;
   int blocklens[17]={6,1,50,1,3,3,15,
                     1,1,1,1,1,1,
                     1,1,1,1};
   MPI_Datatype types[17]={MPI_INT,MPI_INT,MPI_CHAR,MPI_INT,dcomplex_type,dcomplex_type,MPI_CHAR,
                  MPI_INT,MPI_DOUBLE,MPI_DOUBLE,MPI_INT,MPI_INT,MPI_INT,
                  MPI_INT,MPI_INT,MPI_INT,MPI_INT};
   MPI_Aint displacements[17];
   MPI_Datatype broadcast_control_type;

   /* Define a structure for broadcasting the control parameters */
   struct broadcast_control{
      int construction_parameters[6]; /* target construction parameters */
      int from_file; /* target data read from file flag */
      char shape[50]; /* target description string */
      int polarisations; /* number of incident polarisations */
      dcomplex polarisation[3]; /* Incident polarisation state */
      dcomplex orthonormal[3]; /* Orthonormal polarisation state */
      char iterative_scheme[15]; /* iterative scheme */

      int starting_vector; /* number of starting vectors */
      double iterative_tolerance; /* convergence tolerance */
      double iterative_breakdown; /* breakdown tolerance */
      int iterative_maximum; /* maximum number of iterations */
      int iterative_initial_guess; /* initial guess */
      int iterative_precond; /* preconditioning */

      int timing_enabled; /* timing flag */
      int DBLP; /* double output precision */
      int tensor_transpose; /* tensor transpose algorithm */
      int padded_transpose; /* vector transpose algorithm */
   };
   struct broadcast_control control;

   /* Calculate the data offsets fot the MPI_type_struct datatype */
   MPI_Get_address(&control,&displacements[0]);
   MPI_Get_address(&control.from_file,&displacements[1]);
   MPI_Get_address(control.shape,&displacements[2]);
   MPI_Get_address(&control.polarisations,&displacements[3]);
   MPI_Get_address(control.polarisation,&displacements[4]);
   MPI_Get_address(control.orthonormal,&displacements[5]);
   MPI_Get_address(control.iterative_scheme,&displacements[6]);
   MPI_Get_address(&control.starting_vector,&displacements[7]);
   MPI_Get_address(&control.iterative_tolerance,&displacements[8]);
   MPI_Get_address(&control.iterative_breakdown,&displacements[9]);
   MPI_Get_address(&control.iterative_maximum,&displacements[10]);
   MPI_Get_address(&control.iterative_initial_guess,&displacements[11]);
   MPI_Get_address(&control.iterative_precond,&displacements[12]);
   MPI_Get_address(&control.timing_enabled,&displacements[13]);
   MPI_Get_address(&control.DBLP,&displacements[14]);
   MPI_Get_address(&control.tensor_transpose,&displacements[15]);
   MPI_Get_address(&control.padded_transpose,&displacements[16]);
   begin=displacements[0];
   for(i=0;i<count;i++){
      displacements[i]-=begin;
   }

   if(parallel.myrank==0){ /* Restrict to master */
      /* Cram all pertinent data into a single structure for a single broadcast */
      for(i=0;i<6;i++){ /* target description string */
         control.construction_parameters[i]=target.construction_parameters[i];
      }
      control.from_file=target.from_file; /* target data read from file flag */
      for(i=0;i<50;i++){ /* target description string */
         control.shape[i]=target.shape[i];
      }
      control.polarisations=polarisations; /* number of incident polarisations */
      for(i=0;i<3;i++){ /* Incident polarisation state */
         control.polarisation[i]=incident_LF.polarisation[i];
      }
      for(i=0;i<3;i++){ /* Incident polarisation state */
         control.orthonormal[i]=incident_LF.orthonormal[i];
      }
      for(i=0;i<15;i++){ /* iterative scheme */
         control.iterative_scheme[i]=iterative.scheme[i];
      }
      control.starting_vector=iterative.vec; /* number of starting vectors */
      control.iterative_tolerance=iterative.tolerance; /* convergence tolerance */
      control.iterative_breakdown=iterative.breakdown; /* breakdown tolerance */
      control.iterative_maximum=iterative.maximum; /* maximum number of iterations */
      control.iterative_initial_guess=iterative.initial_guess; /* initial guess */
      control.iterative_precond=iterative.precond; /* preconditioning */
      control.timing_enabled=timing.enabled; /* timing flag */
      control.DBLP=DBLP; /* double output precision */
      control.tensor_transpose=parallel.tensor_transpose; /* tensor transpose algorithm */
      control.padded_transpose=parallel.padded_transpose; /* vector transpose algorithm */
   }

   /* Create and commit the MPI Datatype */
   MPI_Type_create_struct(count,blocklens,displacements,types,&broadcast_control_type);
   MPI_Type_commit(&broadcast_control_type);

   /* Broadcast the control parameter structure */
   MPI_Bcast(&control,1,broadcast_control_type,0,dda_mpi_comm);

   if(parallel.myrank!=0){ /* Restrict to slaves */
      /* Extract the data */
      for(i=0;i<6;i++){ /* target description string */
         target.construction_parameters[i]=control.construction_parameters[i];
      }
      target.from_file=control.from_file; /* target data read from file flag */
      for(i=0;i<50;i++){ /* target description string */
         target.shape[i]=control.shape[i];
      }
      polarisations=control.polarisations; /* number of incident polarisations */
      for(i=0;i<3;i++){ /* Incident polarisation state */
         incident_LF.polarisation[i]=control.polarisation[i];
      }
      for(i=0;i<3;i++){ /* Incident polarisation state */
         incident_LF.orthonormal[i]=control.orthonormal[i];
      }
      for(i=0;i<15;i++){ /* iterative scheme */
         iterative.scheme[i]=control.iterative_scheme[i];
      }
      iterative.vec=control.starting_vector; /* number of starting vectors */
      iterative.tolerance=control.iterative_tolerance; /* convergence tolerance */
      iterative.breakdown=control.iterative_breakdown; /* breakdown tolerance */
      iterative.maximum=control.iterative_maximum; /* maximum number of iterations */
      iterative.initial_guess=control.iterative_initial_guess; /* initial guess */
      iterative.precond=control.iterative_precond; /* preconditioning */
      timing.enabled=control.timing_enabled;/* timing flag */
      DBLP=control.DBLP;/* double output precision */
      parallel.tensor_transpose=control.tensor_transpose;/* tensor transpose algorithm */
      parallel.padded_transpose=control.padded_transpose; /* vector transpose algorithm */
   }

   /* Free the MPI Datatype */
   MPI_Type_free(&broadcast_control_type);
}

void broadcast_target_from_file(void){
   /* Broadcast the bitfielded array of 0/1 flags for whether a site is occupied by a dipole or not */
   MPI_Bcast(target.occupied,target.occ_ctrl,MPI_UNSIGNED,0,dda_mpi_comm);
}
