/*
    OpenDDA implementation of interface functionality
    Adapted from OpenDDA -- originaly written by James Mc Donald 2006
    [Antispam: email in reverse] ei.yawlagiun.ti@semaj

    Copyright (C) 2020  Benjamin "Mogli" Mann

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "dda.h"
#include "opendda_MPI.h"


/*
   @param   e0   x and y component of "normalized" Jones vector [E_{x},E_{y},0] (normalization handled here)
*/
void set_incident_polarization(const dcomplex e0[2]){

   /* Set incident direction in the lab frame to +z */
   incident_LF.n_inc[0]=0.0;
   incident_LF.n_inc[1]=0.0;
   incident_LF.n_inc[2]=1.0;

   dcomplex normalise=dcomplex_add(dcomplex_mul(e0[0],dcomplex_conj(e0[0])),
                           dcomplex_mul(e0[1],dcomplex_conj(e0[1])));
   /* Check normalisation value */
   if(MPI_rank() == 0 && normalise.dat[0]<DBL_EPSILON){
      print_error("Incident polarisation description error","The incident polarisation vector e0 CANNOT be the zero vector","");
   }

   normalise.dat[0]=1.0/sqrt(normalise.dat[0]);
   incident_LF.polarisation[0]=dcomplex_scale(e0[0],normalise.dat[0]);
   incident_LF.polarisation[1]=dcomplex_scale(e0[1],normalise.dat[0]);
   incident_LF.polarisation[2] = zero;

   /* Calculate the orthonormal polarisation state */
   /* =======================================================================
      a=[x0,y0,z0],b=[x1,y1,z1]
      a CROSS b = [y0z1-y1z0,x1z0-x0z1,x0y1-x1y0]

      e1=z CROSS conj(e0)
      a=[0,0,z0]=[0,0,1]
      b=[conj(x1),conj(y1),0]
      e1 = a CROSS b = [-conj(y1)z0,conj(x1)z0,0]=[-conj(y1),conj(x1),0] */
   incident_LF.orthonormal[0]=dcomplex_mul(dcomplex_conj(incident_LF.polarisation[1]),minusone); /* x-component */
   incident_LF.orthonormal[1]=dcomplex_conj(incident_LF.polarisation[0]); /* y-component */
}

/*
   @param   domain_shape   either the name of a file containing the shape configuration or one of the predefined shapes (see above)
   @param   domain_size    number of dipoles in [X,Y,Z] direction
*/
void domain_setup(const char* domain_shape, const int domain_size[3]){
   if (MPI_rank() == 0){
      char string[200]={0};
      FILE *data;

      reset_string(target.shape);
      strncpy(target.shape,domain_shape,strlen(domain_shape));
      /* Check to see if the shape identifier is recognised OR is a valid file name */
      if((strcmp(target.shape,"ellipsoid")!=0)&&
         (strcmp(target.shape,"cuboid")!=0)){ /* If not a recognised shape identifier */
         if((data=fopen(target.shape,"r"))!=NULL){ /* Target data file exists */
            target.from_file=1; /* Set flag to indicate that the target data is to be read from a file */
            fclose(data);
         }
         else{ /* The specified identifier is NOT recognised AND is NOT a valid file name */
            reset_string(string);
            sprintf(string,"\'%s\' is not a recognised shape and is not the name of an accessible data file",target.shape);
            print_error("Target shape description error",string,"");
         }
      }
      for (int m = 0; m < 3; ++m){
         int dim = domain_size[m];
         if (dim > 0){
            target.construction_parameters[m] = domain_size[m];
         }
         else{
            print_error("Target shape description error","Domain size MUST be > 0 in each direction","");
         }
      }
   }
}

/* Setup iterative solver.
   @param   iterative_scheme  name of scheme to be used. Choose from the above definitions!
   @param   preconditioning   0: no preconditioning, 1: point-Jacobi-preconditioning
   @param   starting_vectors  number of starting vectors; only used for DDA_ML_BICG_STAB_ORG, DDA_ML_BICG_STAB, DDA_ML_BICG_STAB_SS and DDA_R_BICG_STAB.
*/
void iterative_solver_setup(const char* iterative_scheme, int preconditioning, int starting_vectors){

   char string[200]={0};

   // check input
   if (MPI_rank() == 0){

      if((strcmp(iterative_scheme,"bicg")!=0)&&
         (strcmp(iterative_scheme,"bicg_sym")!=0)&&
         (strcmp(iterative_scheme,"bicgstab")!=0)&&
         (strcmp(iterative_scheme,"cg")!=0)&&
         (strcmp(iterative_scheme,"cgs")!=0)&&
         (strcmp(iterative_scheme,"mlbicgstab_orig")!=0)&&
         (strcmp(iterative_scheme,"mlbicgstab")!=0)&&
         (strcmp(iterative_scheme,"mlbicgstab_ss")!=0)&&
         (strcmp(iterative_scheme,"qmr")!=0)&&
         (strcmp(iterative_scheme,"qmr_sym")!=0)&&
         (strcmp(iterative_scheme,"rbicgstab")!=0)&&
         (strcmp(iterative_scheme,"tfqmr")!=0)){
         reset_string(string);
         sprintf(string,"Scheme identifier \'%s\' not recognised",iterative_scheme);
         print_error("Iterative scheme description error",string,"Please use one of the prediefined schemes!");
      }

      if(((starting_vectors < 1 || starting_vectors > 50) &&(strcmp(iterative_scheme,"mlbicgstab_orig")==0))||
         ((starting_vectors < 1 || starting_vectors > 50) &&(strcmp(iterative_scheme,"mlbicgstab")==0))||
         ((starting_vectors < 1 || starting_vectors > 50) &&(strcmp(iterative_scheme,"mlbicgstab_ss")==0))||
         ((starting_vectors < 1 || starting_vectors > 50) &&(strcmp(iterative_scheme,"rbicgstab")==0))){
         reset_string(string);
         sprintf(string,"The iterative scheme \'%s\' requires a number of starting vectors n=1,...,50",iterative_scheme);
         print_error("Iterative scheme description error",string,"Please add a valid parameter");
      }
   }

   reset_string(iterative.scheme);
   strncpy(iterative.scheme,iterative_scheme,strlen(iterative_scheme));
   iterative.vec = starting_vectors;
   iterative.precond = (preconditioning)? 1 : 0;
}

void dda_init(const MPI_Comm comm, const char* iterative_scheme, int preconditioning, int starting_vectors, const double scatter[2], double wavelength, const dcomplex e0[2], const char* domain_shape, const int domain_size[3], double d, int enable_timing){
   int j,k;
   long int index0i,index0k;

   dda_mpi_comm = comm;
   mpi_initialise();

   set_incident_polarization(e0);

   domain_setup(domain_shape, domain_size);

   if(d < DBL_EPSILON){
         print_error("Description error","Dipole spacing MUST be > 0","");
   }

   // default settings for iterative solver
   iterative_solver_setup(iterative_scheme, preconditioning, starting_vectors);
   dda_configure_iterative_solver(1e-10, 1e-50, 10000, DDA_X0);

   timing.enabled = (enable_timing)? 1 : 0;

   DBLP = 7;

   /* technically there are more choices for these algorithms (0....5).
      We use those algorithms which comply with the MPI standard
   */
   parallel.tensor_transpose = 2;
   parallel.padded_transpose = 2;

   /* Broadcast various control parameters to the slave nodes */
   broadcast_control_parameters();

   if(timing.enabled){ /* Initialise timing */
      timing.overall[0]=MPI_Wtime();
   }

   set_domain(); /* Set K, J and P for the target shape */

   target.M=target.K*target.J; /* M=K*J */
   target.Q=target.K*target.P; /* Q=K*P */
   target.N=target.K*target.J*target.P; /* N=K*J*P */
   target.N3=3*target.N; /* N*3 */

   /* FFTW is best at handling sizes of the form 2^{a}3^{b}5^{c}7^{d}11^{e}13^{f},
   where (e + f) is either 0 or 1, and the other exponents are arbitrary. */
   find_dft_size(target.K,&target.Kp,&target.Kpp,&target.kf);
   find_dft_size(target.J,&target.Jp,&target.Jpp,&target.jf);
   find_dft_size(target.P,&target.Pp,&target.Ppp,&target.pf);

   target.Ka=target.K+target.kf; /* Ka=K+kf */
   target.Ja=target.J+target.jf; /* Ja=J+jf */
   target.Pa=target.P+target.pf; /* Pa=P+pf */
   target.Ma=target.Ja*target.Ka; /* Ma=Ka*Ja */
   target.Qa=target.Ka*target.Pa; /* Qa=Ka*Pa */
   target.Na=target.Ka*target.Ja*target.Pa; /* Na=Ka*Ja*Pa */
   target.Mpp=target.Kpp*target.Jpp; /* Mpp=Kpp*Jpp */
   target.Mv=target.K*target.Jpp; /* Mv=K*Jpp */
   target.Qpp=target.Kpp*target.Ppp; /* Qpp=Kpp*Ppp */
   target.KaJpp=target.Ka*target.Jpp; /* KaJpp=Ka*Jpp */
   target.PKpp=target.P*target.Kpp; /* PKpp=P*Kpp */
   target.Npp=target.Kpp*target.Jpp*target.Ppp; /* Npp=Kpp*Jpp*Ppp */

   /* Initialise bitfielding for target parameterisation */
   bitfielding();

   /* Build the target */
   build_target();
   target.Nd3=3*target.Nd; /* Nd*3 */

   /* ->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->-> */
   /* Determine memory allocation sizes for each node */
   memory_allocation_sizes();

   /* Count the number of occupied lattice sites for each processor */
   for(k=0;k<target.P;k++){
      if(k%parallel.np==parallel.myrank){
         parallel.plane=((int)((double)k/parallel.dnp));
         index0k=k*target.M;
         for(j=0;j<target.M;j++){
            index0i=index0k+j;
            if(target.occupied[(int)((double)index0i/32.0)]&(1<<(index0i%32))){ /* Lattice site is occupied */
               parallel.alloc_vector++; /* Increment the number of occupied lattice sites on this processor */
            }
         }
      }
   }
   parallel.alloc_vector3=parallel.alloc_vector*3;

   /* ->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->-> */
   /* Allocate memory */
   dda_total_memory_usage = memory_allocate_MPI();

   /* To conserve memory array elements that correspond to vacant lattice sites
      and are thus zero are not stored. The populated array stores the starting
      index for the non-zero elements for each of the parallel.planesY_vector
      local xy-planes on each node */
   for(k=0;k<parallel.planesY_vector;k++){
      target.populated[k]=0;
   }
   for(k=0;k<target.P;k++){
      if(k%parallel.np==parallel.myrank){
         parallel.plane=((int)((double)k/parallel.dnp));
         index0k=k*target.M;
         for(j=0;j<target.M;j++){
            index0i=index0k+j;
            if(target.occupied[(int)((double)index0i/32.0)]&(1<<(index0i%32))){ /* Lattice site is occupied */
               target.populated[parallel.plane]++; /* Increment the number of dipoles stored in plane k */
            }
         }
         if(parallel.plane!=0){ /* Add successively */
            target.populated[parallel.plane]+=target.populated[parallel.plane-1];
         }
      }
   }

   for(k=(parallel.planesY_vector-1);k>0;k--){ /* Set starting indices */
      target.populated[k]=target.populated[k-1];
   }
   target.populated[0]=0;

   /* Create the FFTW DFT plans */
   dft_plan(1);

   // check input
   if (MPI_rank() == 0){
      if(scatter[0] < 0 || scatter[0] > 360){
         print_error("Scattering direction error","scatter[0] MUST be in range [0,360]","");
      }
      if(scatter[1] < 0 || scatter[1] > 180){
         print_error("Scattering direction error","scatter[1] MUST be in range [0,180]","");
      }
   }

   /* Calculate the scattering vectors */
   scattering_vectors(scatter[0],scatter[1]);

   /* Rotate the incident polarisation vectors and scattering vectors LF to TF */
   dda_rotate_target(0,0,0);

   // compute dipole spacing and resulting density
   target.dipole_spacing = d;
   dda_density = (3.0 * d*d*d) / (4.0 * dpi);
   // volume=(4/3)*PI*(effective_radius)^{3}=Nd*(dipole_spacing)^{3}
   // where Nd=number of lattice sites occupied by dipoles
   // => effective_radius = d*[(3*Nd)/(4*PI)]^{1/3}
   effective_radius = d * pow(((3.0*(double)target.Nd)/(4.0*dpi)),(1.0/3.0));

   dda_set_wavelength(wavelength);

   // initialize coordinates
   for (int z = 0; z < target.P; ++z)
   {
      if (z % parallel.np == parallel.myrank) /* Plane z is on proc (k%np) */
      {
         parallel.plane = ((int)((double)z / parallel.dnp));
         int element = target.populated[parallel.plane];

         for (int y = 0; y < target.J; ++y)
         {
            for (int x = 0; x < target.K; ++x)
            {
               int index = z*target.M + y*target.K + x;

               if (target.occupied[(int)((double)index / 32.0)] & (1 << (index % 32)))
               {
                  coordinates[3*element]    = x;
                  coordinates[3*element+1]  = y;
                  coordinates[3*element+2]  = z;

                  ++element;
               }
            }
         }
      }
   }

   computation_complete = 0;
   matrix_setup_complete = 0;
}

void dda_free(){
      /* ->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->-> */
   /* Destroy all FFTW DFT plans */
   dft_plan(0);

   /* ->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->-> */
   /* Free allocated memory back to the heap */
   memory_free_MPI();

   /* ->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->-> */
   /* Finalise timing information */
   if(timing.enabled){timing_info();}

   // free MPI data
   mpi_finalise();
}

void dda_rotate_target(double phi, double theta, double psi){
   // check input
   if (MPI_rank() == 0){
      if(phi < 0 || phi > 360){
         print_error("Target orientation error","Euler phi MUST be in range [0,360]","");
      }
      if(theta < 0 || theta > 180){
         print_error("Target orientation error","Euler theta MUST be in range [0,180]","");
      }
      if(psi < 0 || psi > 360){
         print_error("Target orientation error","Euler psi MUST be in range [0,360]","");
      }
   }
   /* Rotate the incident polarisation vectors and scattering vectors LF to TF */
   rotation_euler(phi,theta,psi);

   matrix_setup_complete = 0;
   computation_complete = 0;
}

void dda_set_incident_field(double phi, double theta, double psi, const dcomplex e0_x, const dcomplex e0_y)
{
   dcomplex e0[2] = {e0_x,e0_y};
   set_incident_polarization(e0);
   dda_rotate_target(phi, theta, psi);
}

void dda_set_wavelength(double wavelength){
   // check input
   if (MPI_rank() == 0){
      if(wavelength < DBL_EPSILON){
         print_error("Wavelength description error","Wavelength MUST be > 0","");
      }
   }

   wavenumber=(2.0*dpi)/wavelength;

   /* Calculate the size parameter and the wavenumber
   size_parameter~wavenumber*effective_radius=[2*PI*effective_radius]/wavelength
   where the effective radius is the radius of a sphere of equal volume

   dipoles_per_wavelength=wavelength/dipole_spacing */
   size_parameter=wavenumber*effective_radius;
   target.dipoles_per_wavelength=wavelength/target.dipole_spacing;

   matrix_setup_complete = 0;
   computation_complete = 0;
}

void dda_configure_iterative_solver(double convergence_tol, double breakdown_tol, int max_iter, int initial_guess){

   // check input
   if (MPI_rank() == 0){
      /* Check tolerance value */
      if(convergence_tol<=0.0){
         print_error("Iterative scheme description error","The convergence tolerance MUST be > 0","");
      }

      /* Check tolerance value */
      if(breakdown_tol<=0.0){
         print_error("Iterative scheme description error","The breakdown tolerance MUST be > 0","");
      }

      /* Check tolerance value */
      if(max_iter<=0){
         print_error("Iterative scheme description error","The number of iterations MUST be > 0","");
      }

      /* Check tolerance value */
      if(initial_guess<0 || initial_guess > 3){
         print_error("Iterative scheme description error","Illegal value for initial guess","Please choose one of the predefined settings");
      }
   }

   iterative.tolerance = convergence_tol;
   iterative.breakdown = breakdown_tol;
   iterative.maximum = max_iter;
   iterative.initial_guess = initial_guess;

   computation_complete = 0;
}


void dda_setup_interaction_matrix(dcomplex* epsilon, int orthogonal_polarization){
   polarisation_state = (orthogonal_polarization)? 1 : 0;

   /* Calculate the dipole polarisabilities */
   setup_interaction_matrix_diagonal(epsilon);

   /* Create the first column of the DDA matrix excluding the diagonal */
   dda_interaction_matrix_1st_column_zero_diagonal();

   matrix_setup_complete = 1;
   computation_complete = 0;
}

void dda_set_material_configuration(dcomplex* epsilon){
   /* Calculate the dipole polarisabilities */
   setup_interaction_matrix_diagonal(epsilon);
   computation_complete = 0;
}

const int* dda_get_coordinates()
{
   return coordinates;
}

void dda_compute_cross_section_efficiencies(){
   if(MPI_rank() == 0 && !computation_complete){
      print_error("Uninitialized value error","Computation of cross section requires solving for P first!!","Please call dda_compute_P(), before calling dda_compute_cross_section_efficiencies()!");
   }

   if(parallel.myrank==0){
      efficiencies();
   }
}

/* get cross section
   @param   i = 1,2,3 -> get c_ext, c_abs, c_sca
*/
double get_cross_section(int i)
{
   if(MPI_rank() == 0 && !computation_complete){
      print_error("Uninitialized value error","Computation of cross section requires solving for P first!!","Please call dda_compute_P()!");
   }

   switch (i)
   {
      case 1:
         return cross_section.extinction;

      case 2:
         return cross_section.absorption;

      case 3:
         return cross_section.scattering;

      default:
         return NAN;
   }
}

double dda_get_c_ext()
{
   return get_cross_section(1);
}

double dda_get_c_abs()
{
   return get_cross_section(2);
}

double dda_get_c_sca()
{
   return get_cross_section(3);
}

// custom initial guess
#define DDA_XCUSTOM 4711

dcomplex* dda_get_initialGuessPtr(){
   iterative.initial_guess = DDA_XCUSTOM;
   return dipole_polarisation;
}

const dcomplex* dda_compute_P(int adjoint){
   if(MPI_rank() == 0 && !matrix_setup_complete){
      print_error("Setup error","Computation of polarization requires setting up the interaction matrix first!","Please call dda_setup_interaction_matrix(), before calling dda_compute_P()!");
   }
   /* Calculate the incident electric field at the lattice sites */
   incident_electric_field_MPI(adjoint);

   /* Set the initial guess for the iterative solver */
   if (iterative.initial_guess != DDA_XCUSTOM)
      set_initial_guess();

   /* Solve the linear system
      interaction_matrix*polarisations=incident_E_field */
   iterative_solution();

   // compute cross sections
   cross_sections_MPI();

   computation_complete = 1;
   return dipole_polarisation;
}

const dcomplex* dda_getP(){
   if(MPI_rank() == 0 && !computation_complete){
      print_error("Uninitialized value error","Requires solving for P first!!","Please call dda_compute_P(), before calling dda_getP()!");
   }

   return dipole_polarisation;
}

const dcomplex* dda_getE(int conjugate){
   /* Calculate the incident electric field at the lattice sites */
   incident_electric_field_MPI(conjugate);
   return incident_E_field;
}

double dda_get_density()
{
   return dda_density;
}

int dda_N_local(){
   return parallel.alloc_vector;
}

int dda_N_global(){
   return target.Nd;
}

int MPI_rank(){
   return parallel.myrank;
}

int MPI_np(){
   return parallel.np;
}

size_t dda_local_memory_usage(){
   return dda_total_memory_usage;
}