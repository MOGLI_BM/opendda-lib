/* Written by James Mc Donald 2006
   [Antispam: email in reverse] ei.yawlagiun.ti@semaj

   Function prototypes for read_parameters

   Copyright (C) 2006 James Mc Donald
   Computational Astrophysics Laboratory
   National University of Ireland, Galway
   This code is covered by the GNU General Public License */
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include <math.h>
#include "dcomplex_alloc.h"
#include "dcomplex_math_MPI.h"
#include "definitions.h"
#include "double_alloc.h"
#include "file_alloc.h"
#include "int_alloc.h"
#include "print_details.h"
#include "reset_string.h"

void read_parameters(void);
void get_domain_size(FILE **file_ptr);
