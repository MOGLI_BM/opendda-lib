/*
    Test app for OpenDDA library

    Copyright (C) 2020  Benjamin "Mogli" Mann

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// extern "C" {
#include "dda.h"
// }
#include <vector>
#include <complex>
#include <iostream>
#include <mpi.h>


// complex number
using complex_t = std::complex<double>;
// vector of complex numbers
using C_vec = std::vector<complex_t>;
// vector of complex numbers for OpenDDA
using C_vec_dda = std::vector<dcomplex>;

/*  @param n    number of dipoles
    @param dim  dimension of domain */
void stdcomplex_to_OpenDDAcomplex(const C_vec& src, C_vec_dda& dst, int n, int dim)
{
    // ordering of vector components in OpenDDA: [x1,...xn,y1,...,yn,z1,...,zn]
    int k = 0;

    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < dim; ++j)
        {
            dst[j * n + i].dat[0] = src[k].real();
            dst[j * n + i].dat[1] = src[k].imag();
            ++k;
        }
    }
}

/*  @param n    number of dipoles
    @param dim  dimension of domain */
void OpenDDAcomplex_to_stdcomplex(const dcomplex* src, C_vec& dst, int n, int dim)
{
    // ordering of vector components in OpenDDA: [x1,...xn,y1,...,yn,z1,...,zn]
    int k = 0;

    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < dim; ++j)
        {
            dst[k].real(src[j * n + i].dat[0]);
            dst[k].imag(src[j * n + i].dat[1]);
            ++k;
        }
    }
}


int main(int argc, char** argv)
{
    MPI_Init(&argc, &argv);

    // parameters
    double wavelength = 3.175;
    double d = 0.1;
    double euler[3] = {25.0, 0.5735764364, 80.0};
    double scatter[2] = {45.0, 30.0};
    int domain_size[3] = {30,30,30};
    C_vec e0 = {1,0};
    C_vec_dda e0_dda(2);
    stdcomplex_to_OpenDDAcomplex(e0, e0_dda, 2, 1);


    dda_init(MPI_COMM_WORLD, DDA_BICG_STAB, 1, 0, scatter, wavelength, e0_dda.data(),DDA_ELLIPSOID, domain_size, d, 1);
    dda_rotate_target(euler[0], euler[1], euler[2]);

    const int N = dda_N_local();

    std::cout << "rank " << MPI_rank() << " holds " << N << " dipoles.\n";

    C_vec epsilon(N);
    C_vec_dda epsilon_dda(N);

    C_vec P(3 * N);

    // test with some configuration
    std::fill(epsilon.begin(), epsilon.end(), complex_t(0,1));
    // epsilon[0] = complex_t(1,1e5);
    stdcomplex_to_OpenDDAcomplex(epsilon, epsilon_dda, N, 1);

    dda_setup_interaction_matrix(epsilon_dda.data(), 0);

    dda_compute_P(0);
    // OpenDDAcomplex_to_stdcomplex(dda_getP(), P, N, 3);
    // for (auto& p : P)
    // {
    //     std::cout << std::abs(p) << std::endl;
    // }
    // std::cout << "------------------------------" << std::endl;

    // dda_compute_cross_section_efficiencies();
    if (MPI_rank() == 0)
    {
        std::cout << MPI_rank() << ": cross sections: "
            << dda_get_c_ext()  << ",\t"
            << dda_get_c_abs()  << ",\t"
            << dda_get_c_sca()  << "\n";
    }

    // dda_compute_P(1);
    // OpenDDAcomplex_to_stdcomplex(dda_getP(), P, N, 3);
    // for (auto& p : P)
    // {
    //     std::cout << std::abs(p) << std::endl;
    // }
    // std::cout << "------------------------------" << std::endl;

    // std::fill(epsilon.begin(), epsilon.end(), complex_t(1,1));
    // stdcomplex_to_OpenDDAcomplex(epsilon, epsilon_dda, N, 1);
    // dda_set_material_configuration(epsilon_dda.data());
    // dda_compute_cross_section_efficiencies();

    // std::fill(epsilon.begin(), epsilon.end(), complex_t(0,1));
    // stdcomplex_to_OpenDDAcomplex(epsilon, epsilon_dda, N, 1);
    // dda_set_material_configuration(epsilon_dda.data());
    // dda_compute_cross_section_efficiencies();


    dda_free();
    MPI_Finalize();
    return 0;
}
