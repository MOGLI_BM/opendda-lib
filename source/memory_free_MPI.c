/* Written by James Mc Donald 2006
   [Antispam: email in reverse] ei.yawlagiun.ti@semaj

   Free allocated memory

   Copyright (C) 2006 James Mc Donald,
   Computational Astrophysics Laboratory,
   National University of Ireland, Galway
   This code is covered by the GNU General Public License */
#include "memory_free_MPI.h"

void memory_free_MPI(void){

   int i;

   int_free(&coordinates);

   uint_free(&target.occupied); /* Occupied/Unoccupied bitfielded flag array */
   dcomplex_free(&dipole_polarisation); /* Dipole polarisations */
   dcomplex_free(&incident_E_field); /* Incident electric field */
   dcomplex_fftw_free(&zero_padded_vector); /* Zero-padded vector for DFT matrix-vector multiplication */
   dcomplex_free(&interaction_matrix); /* 1st column of the interaction matrix with zero-diagonal */
   dcomplex_free(&interaction_matrix_diagonal); /* Diagonal of the interaction matrix */

   /* To conserve memory array elements that correspond to vacant lattice sites
      and are thus zero are not stored. The populated array stores the starting
      index for the non-zero elements for each of the parallel.planesY_vector
      local xy-planes on each node */
   int_free(&target.populated);

   /* 1D scratch arrays for the DFT of the interaction_matrix */
   dcomplex_fftw_free(&xdft);  /* xdft[Kpp] */
   dcomplex_fftw_free(&zdft);  /* zdft[Ppp] */

   /* Scratch array for the DFT, FD multiplication & iDFT of the 3 zero-padded vector components */
   dcomplex_fftw_free(&xzscratch); /* xzscratch[3*Qpp] */

   if(iterative.precond==1){ /* Point-Jacobi Preconditioning */
      dcomplex_free(&point_jacobi);
   }

   dcomplex_free(&parallel.local_tensor); /* Temporary scratch array for local transpose */
   dcomplex_free(&parallel.local_padded); /* Temporary scratch array for local transpose */
   int_free(&parallel.transposed_flag_tensor); /* Bit-fielded array for local transpose */
   int_free(&parallel.transposed_flag_padded); /* Bit-fielded array for local transpose */

   if(strcmp(iterative.scheme,"bicg")==0){
      dcomplex_free(&r);
      dcomplex_free(&rtilde);
      dcomplex_free(&p);
      dcomplex_free(&ptilde);
      dcomplex_free(&q);
   }
   else if(strcmp(iterative.scheme,"bicg_sym")==0){
      dcomplex_free(&r);
      dcomplex_free(&p);
   }
   else if(strcmp(iterative.scheme,"bicgstab")==0){
      dcomplex_free(&r);
      dcomplex_free(&rtilde);
      dcomplex_free(&p);
      dcomplex_free(&s);
      dcomplex_free(&v);
   }
   else if(strcmp(iterative.scheme,"cg")==0){
      dcomplex_free(&r);
      dcomplex_free(&p);
   }
   else if(strcmp(iterative.scheme,"cgs")==0){
      dcomplex_free(&r);
      dcomplex_free(&rtilde);
      dcomplex_free(&p);
      dcomplex_free(&u);
      dcomplex_free(&q);
   }
   else if(strcmp(iterative.scheme,"mlbicgstab_orig")==0){
      uint_free(&init); /* From SIMD oriented Fast Mersenne Twister initialisation */
      dcomplex_free(&g);
      dcomplex_free(&omega);
      dcomplex_free(&q);
      if(iterative.vec>1){
         dcomplex_free(&d);
      }
      dcomplex_free(&r);
      dcomplex_free(&u);
      dcomplex_free(&zd);
      dcomplex_free(&zg);
      dcomplex_free(&zomega);
      if(iterative.precond!=0){ /* Preconditioning enabled */
         dcomplex_free(&gtilde);
         dcomplex_free(&utilde);
      }
      dcomplex_free(&c);
   }
   else if(strcmp(iterative.scheme,"mlbicgstab")==0){
      uint_free(&init); /* From SIMD oriented Fast Mersenne Twister initialisation */
      dcomplex_free(&g);
      dcomplex_free(&omega);
      dcomplex_free(&q);
      if(iterative.vec>1){
         dcomplex_free(&d);
      }
      dcomplex_free(&r);
      dcomplex_free(&u);
      dcomplex_free(&zd);
      dcomplex_free(&zg);
      dcomplex_free(&zomega);
      if(iterative.precond!=0){ /* Preconditioning enabled */
         dcomplex_free(&gtilde);
         dcomplex_free(&utilde);
      }
      dcomplex_free(&c);
   }
   else if(strcmp(iterative.scheme,"mlbicgstab_ss")==0){
      uint_free(&init); /* From SIMD oriented Fast Mersenne Twister initialisation */
      dcomplex_free(&g);
      dcomplex_free(&omega);
      dcomplex_free(&q);
      dcomplex_free(&r);
      dcomplex_free(&zg);
      dcomplex_free(&zomega);
      dcomplex_free(&c);
   }
   else if(strcmp(iterative.scheme,"qmr")==0){
      dcomplex_free(&r);
      dcomplex_free(&d);
      dcomplex_free(&p);
      dcomplex_free(&q);
      dcomplex_free(&s);
      dcomplex_free(&vtilde);
      dcomplex_free(&omegatilde);
   }
   else if(strcmp(iterative.scheme,"qmr_sym")==0){
      dcomplex_free(&r);
      dcomplex_free(&p);
      dcomplex_free(&pold);
      dcomplex_free(&v);
      dcomplex_free(&vtilde);
   }
   else if(strcmp(iterative.scheme,"rbicgstab")==0){
      dcomplex_free(&u);
      dcomplex_free(&r);
      dcomplex_free(&rtilde);
      for(i=0;i<iterative.vec;i++){
         dcomplex_free(&tau[i]);
      }
      dcomplex_free_ptr(&tau);
      dcomplex_free(&gam);
      dcomplex_free(&gamp);
      dcomplex_free(&gampp);
      dcomplex_free(&sigma);
   }
   else if(strcmp(iterative.scheme,"tfqmr")==0){
      dcomplex_free(&r);
      dcomplex_free(&rtilde);
      dcomplex_free(&d);
      dcomplex_free(&omega);
      dcomplex_free(&v);
      dcomplex_free(&ya);
      dcomplex_free(&yb);
   }
}
