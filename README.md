## C++ library for solving discrete dipole approximation (DDA).

Apart from the API, this is mostly copied from James Mc Donald's framework OpenDDA.
For the meaning of the different parameters, we refer to James Mc Donald's thesis, "OpenDDA - A Novel High-Performance Computational Framework for the Discrete Dipole Approximation".

### Requirements
* MPI
* CMake
* FFTW3

### Usage
An example of how to use this library can be found in [opendda_MPI.cpp](https://gitlab.com/MOGLI_BM/opendda-lib/-/blob/master/source/examples/opendda_MPI.cpp)
